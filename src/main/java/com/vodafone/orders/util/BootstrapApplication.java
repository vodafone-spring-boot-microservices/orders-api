package com.vodafone.orders.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BootstrapApplication implements CommandLineRunner{
	@Autowired
	private ApplicationContext applicationContext;
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Hello world from Spring Boot !!");
		String[] beans = applicationContext.getBeanDefinitionNames();
		for(String bean: beans) {
			System.out.println("Bean : "+bean);
		}
		
	}

}
